# spirit-animals

### Summary
This app will find the user's spirit animal based on their answers to 10 questions on a scale of 1 to 5. The app determines the user's best match and adds their answers to the list of possible spirit animals.

### Built with
Express, Node.js

### Instructions
Want to know your spirit animal? Navigate to the app's [homepage](https://spirit-animals-finder.herokuapp.com/). 

![home page](images/home.png)

Click *Start Quiz*.

Fill out the blank form. Include data for all fields, otherwise you won't be able to submit.

![blank form](images/blank.png)

![filled form](images/filled.png)

Click *Submit Answers*.

Your results will be displayed in a modal. Clicking *Close* will reset all fields so that the quiz can be taken again.

![results](images/results.png)