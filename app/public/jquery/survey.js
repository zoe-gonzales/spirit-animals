$(document).ready(function(){
    $('#submit').on('click', function() {
        // array to hold quiz answers
        var responses = [];
        // loops through question input and converts to integers
        for (var i=0; i < 9; i++){
            var questionNum = i + 1;
            var questionValue = $('#question' + questionNum).val();
            if (questionValue.length === 21){
                questionValue = parseInt(questionValue.slice(0,-20));
            } else if (questionValue.length === 18) {
                questionValue = parseInt(questionValue.slice(0,-17));
            } else {
                questionValue = parseInt(questionValue);
            }      
            // adding to responses array
            responses.push(questionValue);          
        }
        
        var name = $('#name').val().trim();
        var photo = $('#image').val().trim();
        var summary = $('#summary').val().trim();

        if (!name || !photo || !summary){
            $('.required-field').text('required');
            $('#invalid-text').text('Please complete all required fields.');
            $('#invalid-input').modal('show');
        } else {
            // defining the data that will be posted
            var userInput = {
                name: name,
                photo: photo,
                scores: responses,
                summary: summary
            }

            // posting data & triggering modal
            $.ajax({
                method: 'POST',
                url: '/api/animals',
                data: userInput
            }).then(function(data){
                if (data){
                    // displaying results name, image, and summary within the modal
                    $('#result-animal').text(data.name);
                    $('#results-image').attr('src', data.photo);
                    $('#results-text').text(data.summary);
                    // display matched animal in modal
                    $('#results').modal('show');
                }
            });
        }    
    });

    // closing the results modal resets all fields
    $("#reset").on('click', function() {
        $('#name').val('');
        $('#image').val('');
        $('#summary').val('');

        for (var i=1; i < 11; i++){
            var id = '#question' + i;
            $(id).val('1 (strongly disagree)');
        }
    });
});