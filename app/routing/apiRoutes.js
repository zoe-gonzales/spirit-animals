
var express = require('express');
var router = express.Router();
var animals = require('../data/animals');

router.get('/api/animals', function(req,res){
    res.json(animals);
});

router.post('/api/animals', function(req,res){
    var input = req.body;
    var totalDiff;
    var differences = [];

    // loop thru animals array
    for (var i=0; i < animals.length; i++){
        // loop thru user's scores
        for (var j=0; j < input.scores.length; j++){
            totalDiff = 0;
            // loop thru each animal's scores
            animals[i].scores.forEach(score => {
                // calculate total difference, using the integer value
                var difference = score - input.scores[j];
                totalDiff += Math.abs(difference);
            });
        }
        differences.push(totalDiff);
    }

    // matching animal has the lowest value - finding from differences array
    var value = Math.min.apply(null, differences);
    // locating index of minimum total difference
    var spiritAnimalIndex = differences.indexOf(value);
    // use same index in animals array to define user's spirit animal result
    var spiritAnimal = animals[spiritAnimalIndex];
    // adding user to animals array
    animals.push(input);
    // sending response of true to ajax call
    res.send(spiritAnimal);

});

module.exports = router;