
var express = require('express');
var path = require('path');
var router = express.Router();

// get route to display survey page
router.get('/survey', function(req, res){
    res.sendFile(path.join(__dirname + '/../public/survey.html'));
});

// get route to display home page as default
router.get('/*', function(req, res){
    res.sendFile(path.join(__dirname + '/../public/home.html'));
});

module.exports = router;