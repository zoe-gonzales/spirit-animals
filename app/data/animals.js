// animals object holding all potential animals the user could match with
var animals = [
    {
        name: 'Cat',
        photo: 'https://cdn.pixabay.com/photo/2017/02/20/18/03/cat-2083492_960_720.jpg',
        scores: [5, 3, 2, 4, 3, 5, 1, 4, 5, 5],
        summary: 'Independent and self-confident, you know who you are and what you like. Your never-ending curiosity for life drives you forward in all of your endeavors.'
    },
    {
        name: 'Dog',
        photo: 'https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        scores: [4, 4, 1, 2, 3, 4, 4, 5, 3, 4],
        summary: 'Trustworthy and playful, everyone wants to be your best friend. But what they don’t know is that a friend in you is a friend for life. You persevere despite all odds.'
    },
    {
        name: 'Horse',
        photo: 'https://images.unsplash.com/photo-1519614356164-43acba9cb25d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
        scores: [2, 3, 2, 1, 5, 4, 3, 5, 3, 3],
        summary: 'Others are drawn to you for your passion and drive. You might get emotional from time to time, but you always ultimately find a balance between inner calm and intensity.'
    },
    {
        name: 'Fox',
        photo: 'https://images.unsplash.com/photo-1518526157563-b1ee37a05129?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
        scores: [1, 2, 4, 3, 4, 5, 5, 4, 3, 3],
        summary: 'Some might see you as cunning or even cutthroat - but you possess a remarkable dexterity with complex problems and great awareness of all that surrounds you. You excel in tricky situations that call for immediate action.'
    },
    {
        name: 'Deer',
        photo: 'https://timedotcom.files.wordpress.com/2019/02/zombie-deer-disease.jpg?quality=85',
        scores: [3, 5, 1, 2, 1, 2, 3, 2, 3, 5],
        summary: 'People are often drawn to you for your innocence and and graceful approach to life, but you have a magical side, too. Your sensitivity and intuition are your superpowers - allowing you to adapt to change easier than most.'
    },
    {
        name: 'Owl',
        photo: 'https://kids.sandiegozoo.org/sites/default/files/2017-07/greathornedowl-01.jpg',
        scores: [3, 5, 1, 2, 4, 4, 3, 3, 2],
        summary: 'You see what others don’t, well below the surface of societal expectations and masks. It might not be easily apparent to others, but you approach everything you do with wisdom and intuition. You often pursue change in your life, and inspire others to do the same.'
    },
    {
        name: 'Snake',
        photo: 'http://cdn.24.co.za/files/Cms/General/d/8086/9dc0689211414a048dfc0d9562fd0db9.jpg',
        scores: [1, 4, 4, 1, 4, 5, 5, 3, 5, 1],
        summary: 'Others might initially be intimidated by you until they get to know you better. You change quickly and are always prepared for the next stage in life, however unknown it may be. Excelling at self-care, one of your passions in life is guiding others.'
    },
    {
        name: 'Dolphin',
        photo: 'https://i.ytimg.com/vi/MdRLVgRRce8/maxresdefault.jpg',
        scores: [5, 4, 1, 1, 1, 1, 4, 2, 2, 5],
        summary: 'Team player and class clown at the same time, you see opportunities to connect with others everywhere you go. You care deeply about the people in your life and strive to bring them peace and happiness at every opportunity. As a result, you have many loyal friends.'
    }
];

module.exports = animals;