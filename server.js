
// dependencies
var express = require('express');

// Saving express function to variable
var app = express();

// port that the app will listen on
var PORT = process.env.PORT || 3000;

// code to parse responses
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(__dirname + '/app/public'));

// serving static files
app.use(express.static(__dirname + '/public'));

// api routing
app.use(require('./app/routing/apiRoutes'));

// html routing
app.use(require('./app/routing/htmlRoutes'));

// listening on port
app.listen(PORT, function(){
    console.log(`Listening on port ${PORT}`);
});
